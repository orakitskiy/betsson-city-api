using System;
using System.Linq;
using System.Threading.Tasks;
using Betsson.CityApi.Entities;
using Betsson.CityApi.Infrastructure.Exceptions;
using Betsson.CityApi.Services;
using FluentAssertions;
using NUnit.Framework;

namespace Betsson.CityApi.Tests.ServicesTests
{
    public class SightServiceFixture : ServiceFixtureBase
    {
        SightService service;
        const string TestSightName = "Sight";
        City ParentCity;

        [SetUp]
        public async Task SetUpFixture()
        {
            service = new SightService(DataContext);
            ParentCity = await WithCity();
        }

        [Test]
        public async Task When_getting_all_sights()
        {
            
            var sight = DataContext.Sights.Add(new Sight { CityId = ParentCity.Id, Name = TestSightName }).Entity;
            DataContext.SaveChanges();

            var result = await service.Load(null, null, Context(ParentCity.Id));
            result.Should().HaveCount(1);

            var created = result.First();
            created.Id.Should().Be(sight.Id);
            created.Name.Should().Be(TestSightName);
            created.CityId.Should().Be(ParentCity.Id);
        }

        [Test]
        public async Task When_getting_paged_result()
        {
            var sight = DataContext.Sights.Add(new Sight { CityId = ParentCity.Id, Name = TestSightName }).Entity;
            DataContext.SaveChanges();

            var result = await service.Load(1, 1, Context(ParentCity.Id));
            result.Should().BeEmpty();
        }

        [Test]
        public void When_getting_sights_for_non_existing_city()
        {
            Assert.ThrowsAsync<EntityNotFoundException>(() => service.Load(null, null, Context(ParentCity.Id + 1)));
        }

        [Test]
        public async Task When_getting_sight_by_id()
        {
            var sight = DataContext.Sights.Add(new Sight { CityId = ParentCity.Id, Name = TestSightName }).Entity;
            DataContext.SaveChanges();

            var result = await service.LoadById(sight.Id, Context(ParentCity.Id));
            result.Id.Should().Be(sight.Id);
            result.CityId.Should().Be(ParentCity.Id);
            result.Name.Should().Be(TestSightName);
        }

        [Test]
        public void When_getting_by_id_for_non_existing_city()
        {
            var sight = DataContext.Sights.Add(new Sight { CityId = ParentCity.Id, Name = TestSightName }).Entity;
            DataContext.SaveChanges();

            Assert.ThrowsAsync<EntityNotFoundException>(() => service.LoadById(sight.Id, Context(ParentCity.Id + 1)));
        }

        [Test]
        public async Task When_creating_sight()
        {
            await service.Create(new Sight { Name = TestSightName }, Context(ParentCity.Id));
            var sights = DataContext.Sights.ToArray();
            sights.Should().HaveCount(1);

            var sight = sights.First();
            sight.CityId.Should().Be(ParentCity.Id);
            sight.Name.Should().Be(TestSightName);
        }
        
        [Test]
        public async Task When_creating_sight_and_trying_to_override_city_id()
        {
            var newCity = WithCity();
            await service.Create(new Sight { Name = TestSightName, CityId = newCity.Id }, Context(ParentCity.Id));
            var sights = DataContext.Sights.ToArray();
            sights.Should().HaveCount(1);

            var sight = sights.First();
            sight.CityId.Should().Be(ParentCity.Id);
            sight.Name.Should().Be(TestSightName);
        }
        
        [Test]
        public void When_creating_sight_for_non_existing_city()
        {
            Assert.ThrowsAsync<EntityNotFoundException>(() => service.Create(new Sight { Name = TestSightName }, Context(ParentCity.Id + 1)));
        }

        [Test]
        public async Task When_updating_sight()
        {
            var sight = DataContext.Sights.Add(new Sight { Name = TestSightName, CityId = ParentCity.Id }).Entity;
            await DataContext.SaveChangesAsync();
            
            var newName = Guid.NewGuid().ToString("N");
            await service.Update(sight.Id, new Sight { CityId = ParentCity.Id + 1, Name = newName}, Context(ParentCity.Id));

            var updated = DataContext.Sights.FirstOrDefault(x => x.Id == sight.Id);
            updated.Should().NotBeNull();
            updated.Name.Should().Be(newName);
            updated.CityId.Should().Be(ParentCity.Id);
        }

        [Test]
        public async Task When_deleting_sight()
        {
            var sight = DataContext.Sights.Add(new Sight { Name = TestSightName, CityId = ParentCity.Id }).Entity;
            DataContext.SaveChanges();
            
            await service.Delete(sight.Id, Context(ParentCity.Id));

            var sights = DataContext.Sights.ToArray();
            sights.Should().BeEmpty();
        }

        async Task<City> WithCity()
        {
            var result = DataContext.Cities.Add(new City()).Entity;
            await DataContext.SaveChangesAsync();
            return result;
        }

        ParentInputContext Context(int parentCtyId) => new ParentInputContext { ParentEntityId = parentCtyId };
    }
}