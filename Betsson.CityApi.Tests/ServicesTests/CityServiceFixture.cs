using System.Threading.Tasks;
using Betsson.CityApi.Entities;
using Betsson.CityApi.Infrastructure.Exceptions;
using Betsson.CityApi.Services;
using FluentAssertions;
using NUnit.Framework;

namespace Betsson.CityApi.Tests.ServicesTests
{
    public class CityServiceFixture : ServiceFixtureBase
    {
        CityService service;
        
        [SetUp]
        public void SetUpFixture()
        {
            service = new CityService(DataContext);
        }
        
        [Test]
        public async Task When_getting_all_cities()
        {
            await WithCity();
            await WithCity();
            
            await DataContext.SaveChangesAsync();

            var result = await service.Load(null, null, new InputContext());

            result.Should().HaveCount(2);
        }
        
        [Test]
        public async Task  When_getting_city_by_id()
        {
            var city = await WithCity();

            var result = service.LoadById(city.Id, new InputContext());

            result.Should().NotBeNull();
        }
        
        [Test]
        public async Task When_getting_non_existing_city()
        {
            var city = await WithCity();
            Assert.ThrowsAsync<EntityNotFoundException>(() => service.LoadById(100, new InputContext()));
        }

        [Test]
        public async Task When_creating_city()
        {
            var created = await service.Create(new City { Name = "Test" }, new InputContext());
            created.Id.Should().NotBe(0);
            created.Name.Should().Be("Test");
        }

        public async Task<City> WithCity()
        {
            var city = DataContext.Cities.Add(new City()).Entity;
            await DataContext.SaveChangesAsync();
            return city;
            
        }
    }
}