using System;
using Betsson.CityApi.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace Betsson.CityApi.Infrastructure.Controllers
{
    public interface IParentEntityIdProvider
    {
        int GetParentEntityId(string routePart);
    }

    public class ParentEntityIdProvider : IParentEntityIdProvider
    {
        readonly IHttpContextAccessor httpContextAccessor;

        public ParentEntityIdProvider(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }
        public int GetParentEntityId(string routePart)
        {
            var value = httpContextAccessor.HttpContext.GetRouteValue(routePart)?.ToString();
            if(!int.TryParse(value, out var id))
                throw new InvalidRoutePartException($"Invalid parameter value {routePart}");

            return id;
        }
    }
}