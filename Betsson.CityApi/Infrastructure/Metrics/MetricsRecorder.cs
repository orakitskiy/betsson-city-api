using System;
using Prometheus;

namespace Betsson.CityApi.Infrastructure.Metrics
{
    public class MetricsRecorder
    {
        readonly Counter.Child requestCounter;
        readonly Counter.Child failedRequestsCounter;
        readonly Histogram.Child duration;
        
        public MetricsRecorder(string controller, string action)
        {
            var metricLabels = new[] { "hostname", "action" };
            var metricLabelValues = new[] { Environment.MachineName, $"{controller}+{action}" };

            requestCounter = Prometheus.Metrics.CreateCounter("cityapi_requests_total", "Total requests", metricLabels).Labels(metricLabelValues);
            failedRequestsCounter = Prometheus.Metrics.CreateCounter("cityapi_failed_requests_total", "Total failed requests", metricLabels).Labels(metricLabelValues);
            duration = Prometheus.Metrics.CreateHistogram("cityapi_request_duration", "Request duration", labelNames: metricLabels).Labels(metricLabelValues);
        }

        public void RecordRequest() => requestCounter.Inc();
        public void RecordError() => failedRequestsCounter.Inc();
        public void RecordDuration(double requestDuration) => duration.Observe(requestDuration);

        public static MetricsRecorder For(string controller, string action)
        {
            return new MetricsRecorder(controller, action);
        }
    }
}