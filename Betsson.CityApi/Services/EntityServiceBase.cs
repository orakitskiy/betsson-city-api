using System.Linq;
using System.Threading.Tasks;
using Betsson.CityApi.Entities;
using Betsson.CityApi.Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Betsson.CityApi.Services
{
    public class InputContext
    {
    }
    
    public interface IEntityService<TEntity, TContext>
        where TEntity : EntityBase, new()
        where TContext : InputContext
    {
        Task<TEntity[]> Load(int? skip, int? take, TContext context);
        Task<TEntity> LoadById(int id, TContext context);
        Task<TEntity> Create(TEntity entity, TContext context);
        Task Update(int id, TEntity entity, TContext context);
        Task Delete(int id, TContext context);
    }

    public abstract class EntityServiceBase<TEntity, TContext> : IEntityService<TEntity, TContext>
        where TEntity : EntityBase, new()
        where TContext : InputContext
    {
        protected readonly CitiesContext DbContext;

        protected EntityServiceBase(CitiesContext dbContext)
        {
            this.DbContext = dbContext;
        }
        
        public async Task<TEntity[]> Load(int? skip, int? take, TContext context)
        {
            await BeforeLoad(context);
            return await GetQueryable(DbContext.Set<TEntity>(), context)
                .Skip(skip ?? 0)
                .Take(take ?? 50)
                .ToArrayAsync();
        }

        public async Task<TEntity> LoadById(int id, TContext context)
        {
            await BeforeLoad(context);
            
            var entity = await GetQueryable(DbContext.Set<TEntity>(), context)
                .FirstOrDefaultAsync(e => e.Id == id);
               
            if(entity == null)
                throw new EntityNotFoundException(id, typeof(TEntity));

            return entity;
        }

        public async Task<TEntity> Create(TEntity entity, TContext context)
        {
            await BeforeChange(context);
            
            var created = new TEntity();
            ApplyChanges(entity, created, context);

            DbContext.Set<TEntity>().Add(created);
            await DbContext.SaveChangesAsync();

            return created;
        }

        public async Task Update(int id, TEntity entity, TContext context)
        {
            await BeforeChange(context);
            
            var existing = await LoadById(id, context);
            ApplyChanges(entity, existing, context);
            
            await DbContext.SaveChangesAsync();
        }

        public async Task Delete(int id, TContext context)
        {
            await BeforeChange(context);
            
            var existing = await LoadById(id, context);
            if(existing == null)
                return;

            DbContext.Set<TEntity>().Remove(existing);
            await DbContext.SaveChangesAsync();
        }
        
        protected virtual IQueryable<TEntity> GetQueryable(DbSet<TEntity> set, TContext context)
        {
            return set.OrderBy(x => x.Id);
        }
        
        protected virtual void ApplyChanges(TEntity from, TEntity to, TContext context)
        {
        }
        
        protected virtual Task BeforeLoad(TContext context)
        {
            return Task.CompletedTask;
        }
        
        protected virtual Task BeforeChange(TContext context)
        {
            return Task.CompletedTask;
        }
    }
}