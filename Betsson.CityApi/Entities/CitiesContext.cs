using Microsoft.EntityFrameworkCore;

namespace Betsson.CityApi.Entities
{
    public class CitiesContext : DbContext
    {
        public CitiesContext(DbContextOptions options)
            : base(options)
        {
        }
        
        public DbSet<City> Cities { get; set; }
        
        public DbSet<Sight> Sights { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>()
                .HasMany(e => e.Sights)
                .WithOne()
                .HasForeignKey(e => e.CityId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}