# Betsson City Api

My task was to refactor existing city api received from junior developer.
I tried to show refactoring process in commit history by commiting intermediate
steps.

The very first step was to introduce the tests, to be sure that during the refactoring 
nothing would be broken.

Then I moved all logic from controller to services making controller simply
accepting requests/returning values. The only place where some logic remained was `PatchEntity`.
It's better to have it there, than leaking `JsonPatchDocument<T>` to the service level, from my viewpoint.

Json file is not the best choice for the data backend, because there are problems from 
performance perspective with reading the data from it. In order to get single entity we need 
to load the whole file to memory, not to mention access synchronization.
This problem can be solved in 2 ways. 

1. Introduce own file based backend. You can always draw inspiration from the papers/books on the databases topic.

2. Use tools that have been created by great developers for us. For the sake of simplicity and 
to avoid making any assumptions about environment I decided to use EF InMemory database.

Then I moved to `Sight` service. During implementation I noticed that it was almost identical
to `CitySerivice`. Entity obviously was different and the fact that we needed to consider parent
city when dealing with sights. The class like `EntityServiceBase<T>` helps us a bit, but still
we need to pass information about parent entity identifier from controller to service. That's why I
extended the service with second generic parameter: `EntityServiceBase<TEntity, TContext>`. For
child entity service it's possible to hardcode parent entity type and validation for each child service, but
it's good to pass this responsibility to parent and be sure that it works properly for all 
implementations. That's how `ChildEntityServiceBase<TEntity, TContext, TParent>` was introduced.

Using these classes it became possible to abstract logic from controllers to base one. The only
problem was with named route and the unique constraint for the route name across the whole application.
It was solved by overriding `GetById` method in derived class and applying `Route` attribute with unique name. 

Then according to DRY principle all exception handling logic was moved to `ExceptionFilter` where
all cases with handling `EntityNotFoundException`...etc were gathered. In future if one wants to 
implement custom response for `EntityNotFoundExceptio`, for instance, the only single change will be needed
instead of going through all of the controllers/methods.
Also we need to validate our entities, that's why `ValidationEntityFilter` was introduced. I decided to
keep data annotation based validation rather then writing own ones. 

Finally we need to give our service proper name which should include its purpose.
Actually that should have been the very first step even before unit tests, because
the later one does renaming, the more code will be changed. (You can see the proof of the latter statement
 in the `b2b932e` commit. Too many renamings.)

It's very useful to have some kind of operational dashboard where service stats are displayed. I won't
provide link to any research, but from my experience debugging time is reduced drastically with it.
Instead of digging to logs it's often enough just to look at the charts to understand
whether it's healthy or not and estimate if something has changed recently.
That's prometheus package was added and metrics filter and recorder were implemented.

Thank you.