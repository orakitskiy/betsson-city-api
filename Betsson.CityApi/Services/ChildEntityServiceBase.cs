using System.Threading.Tasks;
using Betsson.CityApi.Entities;
using Betsson.CityApi.Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Betsson.CityApi.Services
{
    public class ParentInputContext : InputContext
    {
        public int ParentEntityId { get; set; }
    }
    
    public abstract class ChildEntityServiceBase<TEntity, TContext, TParent> : EntityServiceBase<TEntity, TContext> 
        where TEntity : EntityBase, new() 
        where TContext : ParentInputContext
        where TParent : EntityBase
    {
        public ChildEntityServiceBase(CitiesContext dbContext) : base(dbContext)
        {
        }

        protected override Task BeforeLoad(TContext context) => ValidateParentExists(context);
        protected override Task BeforeChange(TContext context) => ValidateParentExists(context);

        async Task ValidateParentExists(TContext context)
        {
            var parent = await DbContext.Set<TParent>().FirstOrDefaultAsync(x => x.Id == context.ParentEntityId);
            if (parent == null)
            {
                throw new EntityNotFoundException(context.ParentEntityId, typeof(TParent));
            }
        }
    }
}