using Betsson.CityApi.Entities;

namespace Betsson.CityApi.Services
{
    public class CityService : EntityServiceBase<City, InputContext>
    {
        public CityService(CitiesContext dbContext) : base(dbContext)
        {
        }
        
        protected override void ApplyChanges(City from, City to, InputContext context)
        {
            to.Name = from.Name;
        }
    }
}