using System.Threading.Tasks;
using Betsson.CityApi.Entities;
using Betsson.CityApi.Services;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Betsson.CityApi.Infrastructure.Controllers
{
    public abstract class EntityControllerBase<TEntity, TContext> : Controller
        where TEntity : EntityBase, new()
        where TContext : InputContext, new()
    
    {
        readonly IEntityService<TEntity, TContext> entityService;

        protected EntityControllerBase(IEntityService<TEntity, TContext> entityService)
        {
            this.entityService = entityService;
        }
        
        [HttpGet]
        public async Task<ActionResult<TEntity[]>> GetAll([FromQuery]int? skip, [FromQuery]int? take)
        {
            return Ok(await entityService.Load(skip, take, CreateContext()));
        }
        
        public virtual async Task<ActionResult<TEntity>> GetById(int id)
        {
            return Ok(await entityService.LoadById(id, CreateContext()));
        }
        
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] TEntity entity)
        {
            var result = await entityService.Create(entity, CreateContext());

            return CreatedAtRoute($"{GetType().Name}_GetEntity", new { id = entity.Id }, result);
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateEntity(int id, [FromBody] TEntity entity)
        {
            await entityService.Update(id, entity, CreateContext());
            return Ok();
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchEntity(int id, [FromBody] JsonPatchDocument<TEntity> patchDoc)
        {
            var context = CreateContext();
            var entity = await entityService.LoadById(id, CreateContext());

            // Poor man mapper
            var target = MapForPatch(entity);
            patchDoc.ApplyTo(target, ModelState);

            await entityService.Update(id, target, CreateContext());

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await entityService.Delete(id, CreateContext());
            return Ok();
        }

        // Poor man mapper instead of heavy artillery aka AutoMapper
        TEntity MapForPatch(TEntity tracked) =>
            JsonConvert.DeserializeObject<TEntity>(JsonConvert.SerializeObject(tracked));

        protected abstract TContext CreateContext();
    }
}