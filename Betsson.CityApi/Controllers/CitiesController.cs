﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Betsson.CityApi.Entities;
using Betsson.CityApi.Infrastructure.Controllers;
using Betsson.CityApi.Services;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Betsson.CityApi.Controllers
{
    [Route("api/[controller]")]
    public class CitiesController : EntityControllerBase<City, InputContext>
    {

        public CitiesController(IEntityService<City, InputContext> entityService) 
            : base(entityService)
        {
        }

        [HttpGet("{id}", Name = nameof(CitiesController) + "_GetEntity")]
        public override Task<ActionResult<City>> GetById(int id)
        {
            return base.GetById(id);
        }

        protected override InputContext CreateContext() => new InputContext();
    }
}