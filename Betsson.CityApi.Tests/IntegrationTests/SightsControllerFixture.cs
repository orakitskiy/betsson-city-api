using System.Net;
using System.Threading.Tasks;
using Betsson.CityApi.Entities;
using Betsson.CityApi.Tests.Infrastructure;
using FluentAssertions;
using NUnit.Framework;

namespace Betsson.CityApi.Tests.ControllersTests
{
    public class SightsControllerFixture : ApiFixtureBase
    {
        [Test]
        public async Task When_getting_sights_for_missing_city()
        {
            await Get(SightEndpoint(100));
            AssertStatus(HttpStatusCode.NotFound);
        }

        [Test]
        public async Task When_creating_invalid_sight()
        {
            var city = await WithCity();
            await Post(SightEndpoint(city.Id), new { });
            
            AssertStatus(HttpStatusCode.BadRequest);
        }

        [Test]
        public async Task When_using_invalid_city_id_value()
        {
            await Get("/cities/invalid/sights");
            AssertStatus(HttpStatusCode.BadRequest);
        }

        [Test]
        public async Task When_trying_to_create_sight_with_parent_id()
        {
            var city = await WithCity();
            await Post(SightEndpoint(city.Id), new { Name = "Test", CityId = 10 });
            AssertStatus(HttpStatusCode.Created);

            await Get(SightEndpoint(city.Id));
            AssertOk();
            var result = JsonContent<Sight[]>();
            result.Length.Should().Be(1);
        }

        string SightEndpoint(int cityId, int? sightId = null) =>
            $"/cities/{cityId}/sights/{sightId}";

        async Task<City> WithCity()
        {
            await Post("/cities", new { Name = "City" });
            return JsonContent<City>();
        }
    }
}