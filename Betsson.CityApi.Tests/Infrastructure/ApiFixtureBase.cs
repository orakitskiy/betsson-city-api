using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Betsson.CityApi.Tests.Infrastructure
{
    public class ApiFixtureBase
    {
        Uri baseAddress = new Uri("http://host/");
        HttpClient client;
        HttpStatusCode Status;
        string Content;

        [SetUp]
        public virtual void SetUp()
        {
            var webAppFactory = new CustomWebApplicationFactory();
                
            client = webAppFactory.CreateClient();
        }
        
        protected Task Get(string url) => Transmit(Request(url, HttpMethod.Get));
        
        protected Task Delete(string url) => Transmit(Request(url, HttpMethod.Delete));

        protected Task Post(string url, dynamic data = null) => Transmit(Request(url, HttpMethod.Post, data));

        protected Task Put(string url, dynamic data = null) => Transmit(Request(url, HttpMethod.Put, data));
        
        protected Task Patch(string url, dynamic data) => Transmit(Request(url, new HttpMethod("PATCH"), data));

        protected T JsonContent<T>() =>
            JsonConvert.DeserializeObject<T>(Content);

        async Task Transmit(HttpRequestMessage request)
        {
            var response = await client.SendAsync(request);
            Status = response.StatusCode;

            Content = await (response.Content?.ReadAsStringAsync() ?? Task.FromResult<string>(null));
        }

        HttpRequestMessage Request(string relativeUri, HttpMethod method, dynamic content = null)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(baseAddress, WithPrefix(relativeUri)),
                Method = method
            };

            if (content != null)
                request.Content = new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json");

            return request;
        }

        protected void AssertOk() =>
            AssertStatus(HttpStatusCode.OK);

        protected void AssertStatus(HttpStatusCode statusCode) =>
            Assert.That(Status, Is.EqualTo(statusCode), $"Content:\r\n{Content}");

        string WithPrefix(string part) => $"/api{part}";
        
        class CustomWebApplicationFactory : WebApplicationFactory<Startup>
        {
            protected override IWebHostBuilder CreateWebHostBuilder() => 
                WebHost.CreateDefaultBuilder().UseStartup<Startup>();

            protected override void ConfigureWebHost(IWebHostBuilder builder) => 
                builder.UseContentRoot(".");
        }
    }
}