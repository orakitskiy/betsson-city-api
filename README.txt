Info:
	* Imagine you have received code from a junior developer.

Task: 
	* Update the code as much as you can according to coding standards: SOLID, DRY, patterns, antipatterns, etc.
	* Implement API for Child Resources of the City. For example City can have any amount (list) of Sights to see.
	* Cover with unit tests.
	* Cover with integration tests: input validation, regression tests.

Information:
	* You can use the .NET Framework you prefer (4.0, 4.5, .NET Core) or the libraries you know most (Autofac, Ninject, Dapper, EF, NSubstitute, Moq)
	* It is not mandatory to implement every detail
	* It is required to show the ideas on how to implement the features step by step
	
Additional info:
	* Swagger link: http://localhost:7771/swagger/