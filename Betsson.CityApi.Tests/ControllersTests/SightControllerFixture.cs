using System.Threading.Tasks;
using Betsson.CityApi.Controllers;
using Betsson.CityApi.Entities;
using Betsson.CityApi.Infrastructure.Controllers;
using Betsson.CityApi.Services;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;

namespace Betsson.CityApi.Tests.ControllersTests
{
    public class SightControllerFixture
    {
        readonly IEntityService<Sight, ParentInputContext> sightServiceMock = Substitute.For<IEntityService<Sight, ParentInputContext>>();
        readonly IParentEntityIdProvider parentEntityIdProvider = Substitute.For<IParentEntityIdProvider>();
        SightsController controller;
        
        [SetUp]
        public void SetUp()
        {
            controller = new SightsController(sightServiceMock, parentEntityIdProvider);
            sightServiceMock.ClearReceivedCalls();
        }

        [Test]
        public async Task When_parent_id_is_provided()
        {
            parentEntityIdProvider.GetParentEntityId(Arg.Any<string>()).ReturnsForAnyArgs(42);
            await controller.GetAll(null, null);
            await sightServiceMock.Received(1).Load(
                Arg.Is((int?) null),
                Arg.Is((int?) null),
                Arg.Is<ParentInputContext>(s => s.ParentEntityId == 42));
        }

        [Test]
        public async Task When_skip_take_parameters_are_provided()
        {
            parentEntityIdProvider.GetParentEntityId(Arg.Any<string>()).ReturnsForAnyArgs(42);
            await controller.GetAll(10, 20);
            await sightServiceMock.Received(1).Load(
                Arg.Is(10),
                Arg.Is(20),
                Arg.Any<ParentInputContext>());
        }
    }
}