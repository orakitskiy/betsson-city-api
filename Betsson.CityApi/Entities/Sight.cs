using System.ComponentModel.DataAnnotations;

namespace Betsson.CityApi.Entities
{
    public class Sight : EntityBase
    {
        [Required(ErrorMessage = "You need to provide a name")]
        public string Name { get; set; }
        public int CityId { get; set; }
    }
}