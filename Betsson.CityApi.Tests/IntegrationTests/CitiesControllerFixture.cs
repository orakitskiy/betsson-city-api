using System.Net;
using System.Threading.Tasks;
using Betsson.CityApi.Entities;
using Betsson.CityApi.Tests.Infrastructure;
using FluentAssertions;
using NUnit.Framework;

namespace Betsson.CityApi.Tests.ControllersTests
{
    public class CitiesControllerFixture : ApiFixtureBase
    {
        [Test]
        public async Task When_getting_initial_cities()
        {
            await Get("/cities");
            AssertOk();
        }

        [Test]
        public async Task When_getting_non_existing_city()
        {
            await Get("/cities/1");
            AssertStatus(HttpStatusCode.NotFound);
        }

        [Test()]
        public async Task When_creating_city()
        {
            await Post("/cities", new { name = "Test"});
            AssertStatus(HttpStatusCode.Created);

            var created = JsonContent<City>();
            created.Id.Should().Be(1);
            created.Name.Should().Be("Test");
        }

        [Test]
        public async Task When_getting_city_by_id()
        {
            await Post("/cities", new { name = "Test"});

            await Get("/cities/1");
            AssertOk();
            var city = JsonContent<City>();

            city.Name.Should().Be("Test");
        }

        [Test]
        public async Task When_updating_city()
        {
            await Post("/cities", new { name = "Test"});

            await Put("/cities/1", new {name = "Updated"});
            
            AssertOk();

            await Get("/cities/1");
            var updated = JsonContent<City>();
            updated.Name.Should().Be("Updated");
        }
        
        [Test]
        public async Task When_updating_non_existing_city()
        {
            
            await Put("/cities/1", new {name = "Updated"});
            AssertStatus(HttpStatusCode.NotFound);
        }

        [Test]
        public async Task When_deleting_city()
        {
            await Post("/cities", new { name = "Test"});
            
            await Delete("/cities/1");
            AssertOk();

            await Get("/cities");
            var result = JsonContent<City[]>();
            result.Should().BeEmpty();
        }
        
        [Test]
        public async Task When_deleting_non_existing_city()
        {
            await Delete("/cities/1");
            AssertStatus(HttpStatusCode.NotFound);
        }
        
        [Test]
        public async Task When_patching_city()
        {
            await Post("/cities", new {name = "city"});
            var result = JsonContent<City>();

            await Patch($"/cities/{result.Id}", new[]{ new{ op = "replace", path="/name", value = "Patched"}});
            AssertOk();

            await Get($"/cities/{result.Id}");
            AssertOk();
            result = JsonContent<City>();
            result.Name.Should().Be("Patched");
        }

        [Test]
        public async Task When_patching_city_and_trying_to_change_its_id()
        {
            await Post("/cities", new { name = "city" });
            var result = JsonContent<City>();

            var body = new object[]
            {
                new { op = "replace", path = "/name", value = "Patched" },
                new { op = "replace", path = "/id", value = 10 },
            };
            await Patch($"/cities/{result.Id}", body);
            AssertOk();

            await Get($"/cities/{result.Id}");
            AssertOk();
        }

        [Test]
        public async Task When_creating_invalid_city()
        {
            await Post("/cities", new {});
            AssertStatus(HttpStatusCode.BadRequest);
        }
    }
}