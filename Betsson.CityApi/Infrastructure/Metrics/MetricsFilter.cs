using System.Diagnostics;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Betsson.CityApi.Infrastructure.Metrics
{
    class MetricsFilter : IActionFilter
    {
        const string DurationKey = "RequestDuration";

        public void OnActionExecuting(ActionExecutingContext context)
        {
            context.HttpContext.Items[DurationKey] = Stopwatch.StartNew();
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            var actionDescriptor = (ControllerActionDescriptor)context.ActionDescriptor;

            var requestDuration = (Stopwatch)context.HttpContext.Items[DurationKey];

            var recorder = MetricsRecorder.For(actionDescriptor.ControllerName, actionDescriptor.ActionName);

            recorder.RecordDuration(requestDuration.Elapsed.TotalSeconds);
            recorder.RecordRequest();

            if (context.Exception != null)
                recorder.RecordError();
        }
    }
}