﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Betsson.CityApi.Entities
{
    public class City : EntityBase
    {
        [Required(ErrorMessage = "You need to provide a Name.")]
        [MaxLength(50)]
        public string Name { get; set; }
        
        [JsonIgnore]
        public List<Sight> Sights { get; set; }
    }
}