using System;

namespace Betsson.CityApi.Infrastructure.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException()
        {
            
        }

        public EntityNotFoundException(string message)
            : base(message)
        {
        }

        public EntityNotFoundException(int id, Type type)
            :this($"Entity {type.Name} with Id={id} was not found" )
        {
        }
    }
}