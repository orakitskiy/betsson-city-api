using System.Threading.Tasks;
using Betsson.CityApi.Entities;
using Betsson.CityApi.Infrastructure.Controllers;
using Betsson.CityApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace Betsson.CityApi.Controllers
{
    [Route("api/cities/{city_id}/[controller]")]
    public class SightsController : EntityControllerBase<Sight, ParentInputContext>
    {
        readonly IParentEntityIdProvider parentEntityIdProvider;

        public SightsController(
            IEntityService<Sight, ParentInputContext> entityService,
            IParentEntityIdProvider parentEntityIdProvider) 
            : base(entityService)
        {
            this.parentEntityIdProvider = parentEntityIdProvider;
        }

        [HttpGet("{id}", Name = nameof(SightsController) + "_GetEntity")]
        public override Task<ActionResult<Sight>> GetById(int id)
        {
            return base.GetById(id);
        }

        protected override ParentInputContext CreateContext() =>
            new ParentInputContext
            {
                ParentEntityId = parentEntityIdProvider.GetParentEntityId("city_id")
            };
    }
}