using System.Linq;
using Betsson.CityApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace Betsson.CityApi.Services
{
    public class SightService : ChildEntityServiceBase<Sight, ParentInputContext, City>
    {
        public SightService(CitiesContext dbContext) 
            : base(dbContext)
        {
        }

        protected override void ApplyChanges(Sight from, Sight to, ParentInputContext context)
        {
            to.Name = from.Name;
            to.CityId = context.ParentEntityId;
        }

        protected override IQueryable<Sight> GetQueryable(DbSet<Sight> set, ParentInputContext context)
        {
            return set.Where(x => x.CityId == context.ParentEntityId);
        }
    }
}