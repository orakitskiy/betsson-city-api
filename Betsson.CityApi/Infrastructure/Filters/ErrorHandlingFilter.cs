using Betsson.CityApi.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace Betsson.CityApi.Infrastructure.Filters
{
    public class ErrorHandlingFilter : IExceptionFilter
    {
        readonly ILogger<ErrorHandlingFilter> logger;

        public ErrorHandlingFilter(ILogger<ErrorHandlingFilter> logger)
        {
            this.logger = logger;
        }
        
        public void OnException(ExceptionContext context)
        {
            context.Result = ToResult(context);
            LogException(context);
        }

        IActionResult ToResult(ExceptionContext context)
        {
            switch (context.Exception)
            {
                case EntityNotFoundException e:
                    return new NotFoundObjectResult(e.Message);
                case InvalidRoutePartException e:
                    return new BadRequestObjectResult(e.Message);
                default:
                    return new ObjectResult($"Error processing request. RequestId: {context.HttpContext.TraceIdentifier}") { StatusCode = 500 };
            }
        }

        void LogException(ExceptionContext context)
        {
            logger.LogError(
                context.Exception,
                "Error for Request: {RequestId} with Url: {RequestUrl}",
                context.HttpContext.TraceIdentifier,
                context.HttpContext.Request.GetDisplayUrl());
        }
    }
}