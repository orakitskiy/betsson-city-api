﻿using Betsson.CityApi.Entities;
using Betsson.CityApi.Infrastructure.Controllers;
using Betsson.CityApi.Infrastructure.Filters;
using Betsson.CityApi.Infrastructure.Metrics;
using Betsson.CityApi.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Prometheus;
using Prometheus.Advanced;
using Swashbuckle.AspNetCore.Swagger;

namespace Betsson.CityApi
{
    public class Startup
    {
        public static IConfiguration Configuration;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            DefaultCollectorRegistry.Instance.Clear();
            
            services.AddMvc(option =>
            {
                option.Filters.Add(typeof(MetricsFilter));
                option.Filters.Add(typeof(ValidationEntityFilter));
                option.Filters.Add(typeof(ErrorHandlingFilter));
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });

            services.AddHttpContextAccessor();

            services.AddDbContext<CitiesContext>(o => o.UseInMemoryDatabase("Cities"));
            services.AddScoped<IEntityService<City, InputContext>, CityService>();
            services.AddScoped<IEntityService<Sight, ParentInputContext>, SightService>();
            services.AddScoped<IParentEntityIdProvider, ParentEntityIdProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMetricServer();
            
            app.UseStatusCodePages();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseMvc();

            loggerFactory.AddDebug(LogLevel.Information);
        }
    }
}
