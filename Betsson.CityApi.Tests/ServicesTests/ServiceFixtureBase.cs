using System;
using Betsson.CityApi.Entities;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Betsson.CityApi.Tests.ServicesTests
{
    public class ServiceFixtureBase
    {
        protected CitiesContext DataContext;
        
        [SetUp]
        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<CitiesContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString("N"))
                .Options;
            
            DataContext = new CitiesContext(options);
        }

        public void TearDown()
        {
            DataContext.Dispose();
        }
    }
}