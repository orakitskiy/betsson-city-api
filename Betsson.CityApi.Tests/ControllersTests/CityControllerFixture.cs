using System.Threading.Tasks;
using Betsson.CityApi.Controllers;
using Betsson.CityApi.Entities;
using Betsson.CityApi.Services;
using NSubstitute;
using NUnit.Framework;


namespace Betsson.CityApi.Tests.ControllersTests
{
    public class CityControllerFixture
    {
        readonly IEntityService<City, InputContext> cityServiceMock = Substitute.For<IEntityService<City, InputContext>>();
        CitiesController controller;
        
        [SetUp]
        public void SetUp()
        {
            controller = new CitiesController(cityServiceMock);
            cityServiceMock.ClearReceivedCalls();
        }

        [Test]
        public async Task When_getting_all()
        {
            await controller.GetAll(10, 10);

            await cityServiceMock.Received(1).Load(Arg.Is(10), Arg.Is(10), Arg.Any<InputContext>());
        }
        
        [Test]
        public async Task When_getting_by_id()
        {
            await controller.GetById(100);

            await cityServiceMock.Received(1).LoadById(Arg.Is(100), Arg.Any<InputContext>());
        }

        [Test]
        public async Task When_creating_city()
        {
            await controller.Create(new City{ Name = "Test"});

            await cityServiceMock.Received(1).Create(Arg.Is<City>(s => s.Name.Equals("Test")), Arg.Any<InputContext>());
        }

        [Test]
        public async Task When_deleting_city()
        {
            await controller.Delete(100);
            await cityServiceMock.Received(1).Delete(100, Arg.Any<InputContext>());
        }
    }
}