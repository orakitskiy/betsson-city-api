using System.ComponentModel.DataAnnotations;

namespace Betsson.CityApi.Entities
{
    public class EntityBase
    {
        [Key]
        public int Id { get; set; }
    }
}