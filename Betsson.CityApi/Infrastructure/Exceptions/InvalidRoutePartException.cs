using System;

namespace Betsson.CityApi.Infrastructure.Exceptions
{
    public class InvalidRoutePartException : Exception
    {
        public InvalidRoutePartException(string message)
            : base(message)
        {
        }
    }
}